from threading   import Semaphore
from queue       import Queue
from threading   import Thread

import time, random

class User:
    def __init__(self, name, id, money = 1000):
        self.id = id
        self.name = name
        self.money = money

        self.lots = {'Хомяк': 0, 'Колесо': 0}

        #self.inpSemph = Semaphore() #Для входной очереди.
        #self.outSemph = Semaphore() #Для выходной очереди.
        
        self.inpEvents = Queue(10) #События от игрока.
        self.outEvents = Queue(10) #События к игроку.

    #----------------------------------------
    #Методы, используемые Worker'ом.
    #----------------------------------------

    #Отправить событие к игроку.
    def Put(self, event):
        #self.outSemph.acquire()

        res = True

        try:
            self.outEvents.put_nowait(event)
        except:
            res = False #Тот случай, когда очередь переполнена. TODO: Отключение игрока. Таймауты.
    
        #self.outSemph.release()

        return res

    #Получить одно событие от игрока.
    def Get(self):
        #self.inpSemph.acquire()

        event = None
        try:
            event = self.inpEvents.get_nowait()
        except:
            pass  #Тот случай, когда очередь пуста. TODO: Таймауты.

        #self.inpSemph.acquire()

        return event
    
    #----------------------------------------



    #----------------------------------------
    #Методы, используемые сервером.
    #----------------------------------------

    #Доставляет события от игрока.
    def PutUserEvent(self, event):
        #self.inpSemph.acquire()

        try:
            self.inpEvents.put_nowait(event)
        except:
            pass #Тот случай, когда очередь переполнена. TODO: Отключение игрока. Таймауты.
    
        #self.inpSemph.release()

    #Заби(е)рает события к игроку.
    def GetUserEvents(self):
        #self.outSemph.acquire()

        events = []
        while not self.outEvents.empty():
            events.append(self.outEvents.get_nowait())

        #self.outSemph.release()

        return events
    #----------------------------------------

    def Raw(self):
        return { 'id': self.id, 'name': self.name, 'money': self.money, 'lots': self.lots }

class Event:
    RoomCreated = { 'id': 0, 'name' : 'room_created'}
    YourMove    = { 'id': 1, 'name' : 'your_move'}
    UserMove    = { 'id': 2, 'name' : 'user_move'}
    UserExit    = { 'id': 6, 'name' : 'user_exited'}
    GameEnd     = { 'id': 7, 'name' : 'game_end'}

    BadRequest  = { 'id': 3, 'name' : 'bad_request'}
    BadParam    = { 'id': 4, 'name' : 'bad_param'}

    NotEnoughMoney = { 'id': 5, 'name' : 'not_enough_money'}



    def __init__(self, body, data):
        self.id   = body['id']
        self.name = body['name']
        self.data = data

    def Raw(self):
        return {'id': self.id, 'name': self.name, 'data': self.data}

class Lot:
    def __init__(self, name, desc, minCost, minStep):
        self.name = name
        self.desc = desc
        self.minCost = minCost
        self.minStep = minStep

    def Raw(self):
        return {'name': self.name, 'desc': self.desc, 'minCost':self.minCost, 'minStep': self.minStep }

class Room:
    def __init__(self, users, lots, count = 0):
        self.lots    = lots
        self.currLot = lots[count]
        self.count   = count
        
        self.users = users
        self.isUserActive = [True for i in range(len(users))]

        self.currCost = self.currLot.minCost

        self.currUser = random.randint(0, len(users) - 1)

        self._SendEventToAllUsers(Event(Event.RoomCreated, { 'lot': self.currLot.Raw() }))

        for i, user in enumerate(self.users):
            if user.money < self.currLot.minCost + self.currLot.minStep:
                self._SendEventToUser(Event(Event.NotEnoughMoney, None), i)
                self.isUserActive[i] = False
                

        if not self._FindNextMoveUser():
            return self._EndGame()

        self._SendEventToUser(Event(Event.YourMove, None), self.currUser)

    def Calc(self):
        event = self.users[self.currUser].Get()

        
        #if event is not None:
        #    print(self.isUserActive, self.users[self.currUser].id, event.Raw())
        #else:
        #    print(self.isUserActive, self.users[self.currUser].id, event)
        

        if event is None:
            return self

        elif event.id == Event.UserMove['id']:
            if event.data['bet'] == 0:
                self.isUserActive[self.currUser] = False
                
                if not self._FindNextMoveUser():
                    return self._EndGame()

                if(self._CheckForWinner()):
                    return self._EndGame()

                self._SendEventToUser(Event(Event.YourMove, None), self.currUser)
                
                return self

            if event.data['bet'] < self.currCost + self.currLot.minStep:
                self._SendEventToUser(Event(Event.BadParam, {'min': self.currLot.minStep + self.currCost}), self.currUser)

                return self

            if event.data['bet'] > self.users[self.currUser].money:
                self._SendEventToUser(Event(Event.NotEnoughMoney, {'money':self.users[self.currUser].money}), self.currUser)
                    
                return self
        
            self.currCost = event.data['bet']
            
            if not self._FindNextMoveUser():
                return self._EndGame()

            self._SendEventToAllUsers(Event(Event.UserMove, {"bet": self.currCost}))

            self._SendEventToUser(Event(Event.YourMove, None), self.currUser)

            return self
        else:
            self._SendEventToUser(Event(Event.BadRequest, None))

            return self

    def _SendEventToAllUsers(self, event):
        for i in range(len(self.users)):
            self._SendEventToUser(event, i)

    def _SendEventToUser(self, event, i):
        if self.isUserActive[i] and not self.users[i].Put(event):
            self.isUserActive[i] = False

        return self.isUserActive[i]

    def _FindNextMoveUser(self):
        for i in range(self.currUser + 1, len(self.users)):
            if(self.isUserActive[i]):
                self.currUser = i
                return True
        
        for i in range(self.currUser):
            if(self.isUserActive[i]):
                self.currUser = i
                return True

        return False

    def _CheckForWinner(self):
        user = None

        for i in range(len(self.users)):
            if self.isUserActive[i]:
               if user is None:
                   user = i
               else:
                   return False

        return True

    def _EndGame(self):
        vin_u = 0

        if(self.users[self.currUser].money >= self.currCost):
            self.users[self.currUser].money -= self.currCost
        
            self.users[self.currUser].lots[self.currLot.name] += 1

            vin_u = self.users[self.currUser].id
        

        self.isUserActive = [True for i in range(len(self.users))]

        self._SendEventToAllUsers(Event(Event.GameEnd, {"user": vin_u}))

        if len(self.lots) == self.count + 1:
            return None

        return Room(self.users, self.lots, self.count + 1)


class UsersManager:
    def __init__(self):
        self.semph = Semaphore()
        self.users = {}

    def Get(self, id):
        self.semph.acquire()

        user = self.users.get(id, None)

        self.semph.release()

        return user

    def Update(self, id, user):
        self.semph.acquire()

        self.users[id] = user

        self.semph.release()

    def Delete(self, id):
        self.semph.acquire()

        self.users.pop(id, None)

        self.semph.release()

class Worker(Thread):
    def __init__(self):
        Thread.__init__(self)

        self.rooms = Queue(100)
        self.work  = True

        self.name = 'Worker.'

        #print(self.semph._cond, self.semph._value)

    #Основной цикл. Работает в отдельном потоке.
    def run(self):

        while self.work:
            room = None

            try:
                room = self.rooms.get(True, 1)
            except:
                continue
            
            try:
                room = room.Calc()
            except Exception as e:
                raise e
            
            #print(room)

            #Если нужно продолжить обработку данной комнаты.
            if(room is not None):
                try:
                    self.rooms.put_nowait(room)
                except:
                    raise Exception()
                    pass #Переполнение комнат. TODO: Отключение комнаты.
            time.sleep(0.1)
    def Stop(self):
        self.work = False

    def AddRoom(self, room):
        try:
            self.rooms.put_nowait(room)
        except:
            pass #Переполнение комнат. TODO: Отключение комнаты.

class Server:
    def __init__(self):
        self.worker  = Worker()
        self.semph   = Semaphore()
        self.userMng = UsersManager()

        self.lots = [   {'name': 'Хомяк', 'cost': 10, 'step': 50, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 100, 'step': 1, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 10, 'step': 10, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 15, 'step': 3, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 30, 'step': 1, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 39, 'step': 3, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 90, 'step': 3, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Хомяк', 'cost': 20, 'step': 3, 'desc': 'Хомяк обыкновенный. 1 зернышко в день = 10 МВт энергии. Срок жизни - вечность.'},
                        {'name': 'Колесо', 'cost': 50, 'step': 14, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 23, 'step': 19, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 50, 'step': 1, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 100, 'step': 4, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 160, 'step': 5, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 40, 'step': 30, 'desc': 'Литьё, 23 дюйма.'}, 
                        {'name': 'Колесо', 'cost': 76, 'step': 30, 'desc': 'Литьё, 23 дюйма.'},
                        {'name': 'Колесо', 'cost': 30, 'step': 9, 'desc': 'Литьё, 23 дюйма.'} ]

        random.shuffle(self.lots)

        self.usersInQueue = []

    def Start(self):
        self.worker.start()
    
    def OnRegUser(self, user_id, name):
        user = self.userMng.Get(user_id)

        if user is not None:
            return {'status': False, 'msg': 'User already exist'}

        user = User(name, user_id)

        self.userMng.Update(user_id, user)
        
        return {'status': True,'msg': 'OK'}

    def OnGetUserStat(self, user_id):
        user = self.userMng.Get(user_id)

        if user is None:
            return {'status': False, 'msg': 'User not found'}

        return {'status': True,'msg': 'OK', 'data': user.Raw() }

    def OnUserDisc(self, user_id):
        user = self.userMng.Get(user_id)

        if user is None:
            return {'status': False, 'msg': 'User not found'}

        self.semph.acquire()
        
        try:

            self.usersInQueue.remove(user)

        except ValueError:
            return {'status': False,'msg': 'OK'}
        finally:
            self.semph.release()

        return {'status': True,'msg': 'OK'}

    def OnNewGameRequest(self, user_id):
        user = self.userMng.Get(user_id)

        if user is None:
            return {'status': False, 'msg': 'User not found'}

        #if user.isPlaying:
        #    return (False, "User is already playing")

        self.semph.acquire()
        
        self.usersInQueue.append(user)

        if(len(self.usersInQueue) >= 3):
            if len(self.lots) == 0:
                self.semph.release()

                return {'status': False,'msg': 'No lots'}
            
            lots = []

            for i in range(10):
                lot = random.choice(self.lots)

                lots.append(Lot(lot['name'], lot['desc'], lot['cost'], lot['step']))

            self.worker.AddRoom(Room(self.usersInQueue, lots))
            
            self.usersInQueue = []
        
        self.semph.release()

        return {'status': True,'msg': 'OK'}

    def OnUpdateRequest(self, id):
        user = self.userMng.Get(id)

        if user is None:
            return {'status': False, 'msg': 'User not found'}

        return {'status': True,'msg': 'OK', 'events': [event.Raw() for event in user.GetUserEvents()]}
    
    def OnAnswerRequest(self, id, event):
        user = self.userMng.Get(id)

        if user is None:
            return {'status': False, 'msg': 'User not found'}

        #Было бы круто проверять на валидность event.

        user.PutUserEvent(Event(event, event['data']))

        return {'status': True,'msg': 'OK'}


