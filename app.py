"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""

from server import Server
from flask  import Flask, abort, request, jsonify

import json

app = Flask(__name__)
serv = Server()

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app

@app.route('/Cancel')
def Cancel():
    id = request.args.get('id', None)

    if id is None:
        abort(400)
    
    return  jsonify(serv.OnUserDisc(int(id)))

@app.route('/RegUser', methods = ['GET', 'POST'])
def RegUser():
    id = request.args.get('id', None)
    name = request.args.get('name', None)

    if id is None or name is None:
        abort(400)

    return  jsonify(serv.OnRegUser(int(id), name))

@app.route('/GetUserStat')
def GetUserStat():
    id = request.args.get('id', None)

    if id is None:
        abort(400)

    return  jsonify(serv.OnGetUserStat(int(id)))

@app.route('/NewGameRequest')
def NewGameRequest():
    id = request.args.get('id', None)

    if id is None:
        abort(400)

    return jsonify(serv.OnNewGameRequest(int(id)))


@app.route('/UpdateRequest')
def UpdateRequest():
    id = request.args.get('id', None)

    if id is None:
        abort(400)

    a = serv.OnUpdateRequest(int(id))

    #print(">>>>", a)

    return jsonify(a)


@app.route('/AnswerRequest', methods = ['POST'])
def AnswerRequest():
    data = json.loads(request.data.decode())

    id    = data.get('id', None)
    event = data.get('event', None)

    if id is None or event is None:
        abort(400)

    return jsonify(serv.OnAnswerRequest(id, event))

if __name__ == '__main__':
    import OSAtBot
    
    import os

    HOST = os.environ.get('SERVER_HOST', 'localhost')
    PORT = 0

    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555

    serv.Start()
    
    OSAtBot.Bot(open('token.conf').readline(), 'https://%s:%i/' % (HOST, PORT)).run()
    
    app.run(HOST, PORT, ssl_context='adhoc')
