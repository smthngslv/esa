from telegram     import ReplyKeyboardMarkup, ReplyKeyboardRemove, KeyboardButton, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Filters, MessageHandler, CallbackQueryHandler, Updater

from threading    import Thread
from time         import sleep, time

import requests, threading

class Event:
    RoomCreated = { 'id': 0, 'name' : 'room_created'}
    YourMove    = { 'id': 1, 'name' : 'your_move'}
    UserMove    = { 'id': 2, 'name' : 'user_move'}
    UserExit    = { 'id': 6, 'name' : 'user_exited'}
    GameEnd     = { 'id': 7, 'name' : 'game_end'}

    BadRequest  = { 'id': 3, 'name' : 'bad_request'}
    BadParam    = { 'id': 4, 'name' : 'bad_param'}

    NotEnoughMoney = { 'id': 5, 'name' : 'not_enough_money'}



    def __init__(self, body, data):
        self.id   = body['id']
        self.name = body['name']
        self.data = data

    def Raw(self):
        return {'id': self.id, 'name': self.name, 'data': self.data}


class User:
    def __init__(self, chat_id):
        self.isExcInp = False
        self.chat_id = chat_id

class Bot:
    class Commands:
        start     = '/start'
        startGame = 'Начать игру'
        help      = 'Помощь'
        stat      = 'Статистика'
    def __init__(self, token, host):
        self.api = host

        print(self.api)

        self.users = {}

        

        self.updater = Updater(token)
        self.updater.dispatcher.add_handler(MessageHandler(Filters.all, lambda *a, **kw: Thread(target = self._baseHandler, args = a, kwargs = kw).start()))
        
        self.updater.dispatcher.add_error_handler(self._Exc)

        self.keyboards = {'main': [[Bot.Commands.startGame],
                                   [Bot.Commands.help,     
                                    Bot.Commands.stat]],
                          'game': [[Bot.Commands.stat]]}

    def run(self):
        self.updater.start_polling()

    def _req(self, method, params, m = 'get'):
        r = None
        
        try:
            if m == 'get':
                r = requests.get(self.api + method, params, verify=False)
            if m == 'post':
                r = requests.post(self.api + method, json = params, verify=False)
        except Exception as err:
            print(err)

            return None
        else:
            return r.json()

    def _getEvents(self, id):
        timeOut = 5000
        
        while timeOut > 0:
            ts = time()

            r = self._req('UpdateRequest', {'id': id})

            if(r is not None and len(r['events']) > 0):
                return r['events']

            sleep(0.5)

            timeOut -= time() - ts

        return None
     
    def _apiEventHandler(self, bot, user, events):
        if events is None:
            bot.send_message(chat_id = user.chat_id, text = 'Сервер не ответил, попробуй зайти позже.', reply_markup = ReplyKeyboardRemove())

            return
        
        for event in events:
            if event['id'] == Event.RoomCreated['id']:
                bot.send_message(chat_id = user.chat_id, text = 'Игра началась!\n\nЛот 🐹🎡 : %s\nОписание: %s\n\nСтавка: %i\nПорог повышения: %i' % (event['data']['lot']['name'], 
                                                                                                                                                event['data']['lot']['desc'],
                                                                                                                                                event['data']['lot']['minCost'],
                                                                                                                                                event['data']['lot']['minStep']), reply_markup = ReplyKeyboardMarkup(self.keyboards['game'], True, False))

            elif event['id'] == Event.YourMove['id']:
                bot.send_message(chat_id = user.chat_id, text = 'Твой ход, напиши целое число или 0, если ты пас.')
                
                user.isExcInp = True

                return

            elif event['id'] == Event.UserMove['id']:
                bot.send_message(chat_id = user.chat_id, text = 'Новая ставка: %i' %event['data']['bet'])
               
            elif event['id'] == Event.GameEnd['id']:
                if event['data']['user'] == user.chat_id:
                    bot.send_message(chat_id = user.chat_id, text = 'Вы выиграли!')

                bot.send_message(chat_id = user.chat_id, text = 'Аукцион завершён: 🐹🎡', reply_markup = ReplyKeyboardMarkup(self.keyboards['main'], True, False))
            elif event['id'] == Event.BadParam['id']:
                bot.send_message(chat_id = user.chat_id, text = 'Хей, следующая минимальная ставка: %i' % event['data']['min'])
                
                user.isExcInp = True

                return

            elif event['id'] == Event.NotEnoughMoney['id']:
                bot.send_message(chat_id = user.chat_id, text = 'Хей, у тебя мало денюжек: %i' % event['data']['money'])
                
                user.isExcInp = True

                return

            else:
                print(event)

        self._apiEventHandler(bot, user, self._getEvents(user.chat_id))
    
    def _Exc(self, bot, update, exc):
        print(exc)

    def _baseHandler(self, bot, update):
        user = self.users.get(update.message.chat.id, None)

        if user is None:
            user = User(update.message.chat.id); self.users[user.chat_id] = user

        msg = update.message.text.strip()

        if   msg == Bot.Commands.start:
            r = self._req('RegUser', {'id': user.chat_id, 'name': update.message.chat.first_name})

            if r is None:
                bot.send_message(chat_id = user.chat_id, text = 'Возникли какие-то неполадки((')
            elif(r['status']):
                bot.send_message(chat_id = user.chat_id, text = 'Хей, привет))',                 reply_markup = ReplyKeyboardMarkup(self.keyboards['main'], True, False))
            else:
                bot.send_message(chat_id = user.chat_id, text = 'Привет, с возвращением))', reply_markup = ReplyKeyboardMarkup(self.keyboards['main'], True, False))

                r = self._req('Cancel', {'id': user.chat_id}) 

                if(r['status']):
                    bot.send_message(chat_id = user.chat_id, text = 'Ты снят с очереди на игру.')

        elif msg == Bot.Commands.startGame:
            r = self._req('Cancel', {'id': user.chat_id}) 

            if(r['status']):
                bot.send_message(chat_id = user.chat_id, text = 'Ты снят с очереди на игру.')

            r = self._req('NewGameRequest', {'id': user.chat_id})

            if r is None:
                bot.send_message(chat_id = user.chat_id, text = 'Возникли какие-то неполадки.')
            elif(r['status']):
                bot.send_message(chat_id = user.chat_id, text = 'Идет поиск игры...',                           reply_markup = ReplyKeyboardRemove())

                self._apiEventHandler(bot, user, self._getEvents(user.chat_id))

            else:
                bot.send_message(chat_id = user.chat_id, text = 'Хм, не могу тебя найти. Напиши /start',        reply_markup = ReplyKeyboardRemove())
        elif msg == Bot.Commands.help:
            h = 'Пользователю предлагается на выбор две вещи: Хомячок и Колесо. Каждый хомяк может вырабатывать электричество, для этого ему нужно колесо. Хомяк без колеса или колесо без хомяка бесполезно. Купите как можно больше пар (Хомяк, Колесо) за наименьшую цену.'

            bot.send_message(chat_id = user.chat_id, text = h)

        elif msg == Bot.Commands.stat:
            r = self._req('GetUserStat', {'id': user.chat_id})

            if r is None:
                bot.send_message(chat_id = user.chat_id, text = 'Возникли какие-то неполадки.')
            elif(r['status']):
                p = min(r['data']['lots']['Хомяк'], r['data']['lots']['Колесо']) * 312.2313 + r['data']['money'] * 1.2987872

                bot.send_message(chat_id = user.chat_id, text = 'Статистика 🐹🎡:\n\nID: %i\nИмя: %s\nДенюжки: %i\nХомячки 🐹: %i\nКолесо 🎡: %i\nОчки: %i' % (r['data']['id'], r['data']['name'], r['data']['money'], r['data']['lots']['Хомяк'], r['data']['lots']['Колесо'], p))
                
        elif user.isExcInp and msg.isdigit():
            r = self._req('AnswerRequest', {'id': user.chat_id, 'event': Event(Event.UserMove, {'bet': int(msg)}).Raw()}, 'post')

            if r is None:
                bot.send_message(chat_id = user.chat_id, text = 'Возникли какие-то неполадки.')
            elif(r['status']):
                bot.send_message(chat_id = user.chat_id, text = 'Твоя ставка: ' + msg)

                if int(msg) == 0:
                    bot.send_message(chat_id = user.chat_id, text = 'Вы пасанули. Ждем следующего лота.')

                user.isExcInp = False

                self._apiEventHandler(bot, user, self._getEvents(user.chat_id))

        else:
            bot.send_message(chat_id = user.chat_id, text = 'Ты делаешь что-то не то...')

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
 
if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
    Bot(open('token.conf').readline()).run()